module gitlab.com/bloom42/bloom/core

go 1.14

require (
	github.com/jmoiron/sqlx v1.2.0
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	gitlab.com/bloom42/bloom/common v0.0.0-20200316201022-5439e3dc72a2
	gitlab.com/bloom42/lily v0.0.0-20200325134955-a6b455af715f
	golang.org/x/crypto v0.0.0-20200302210943-78000ba7a073
	golang.org/x/sys v0.0.0-20200302150141-5c8b2ff67527 // indirect
	google.golang.org/appengine v1.6.5 // indirect
)
