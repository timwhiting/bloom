export default {
  landing: {
    mission: 'Our mission: Empowering people',
    subtitle1: 'Chat, Collaborate, Discover, Innovate.',
    subtitle2: 'Take back control with <span class="blm-rounded-elegance"><b>Bloom</b></span>',
    explore_features: 'Découvrez toutes les fonctionnalités',
  },
  other_downloads: 'Tous les Téléchargements',
};
