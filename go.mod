module gitlab.com/bloom42/bloom

go 1.14

require (
	github.com/99designs/gqlgen v0.11.2
	github.com/getsentry/sentry-go v0.5.1
	github.com/go-chi/chi v4.0.3+incompatible
	github.com/go-chi/cors v1.0.0
	github.com/golang-migrate/migrate/v4 v4.9.1
	github.com/google/uuid v1.1.1
	github.com/gorilla/websocket v1.4.1 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.3.0
	github.com/mattn/go-sqlite3 v2.0.3+incompatible // indirect
	github.com/spf13/cobra v0.0.6
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/stripe/stripe-go v70.2.0+incompatible
	github.com/vektah/gqlparser v1.3.1
	github.com/vektah/gqlparser/v2 v2.0.1
	gitlab.com/bloom42/bloom/common v0.0.0-20200316201022-5439e3dc72a2
	gitlab.com/bloom42/libs/crypto42-go v0.0.0-20200302110011-301af310ee41
	gitlab.com/bloom42/libs/rz-go v1.3.0
	golang.org/x/crypto v0.0.0-20200302210943-78000ba7a073 // indirect
	golang.org/x/sys v0.0.0-20200302150141-5c8b2ff67527 // indirect
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
