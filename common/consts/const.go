package consts

const (
	ENV_PRODUCTION  = "production"
	ENV_STAGING     = "staging"
	ENV_DEVELOPMENT = "dev"

	EMAIL_HELLO = "hello@bloom.sh"

	API_BASE_URL = "http://192.168.1.53:8000/api"
)
